var default_conf = {
    "host" : "", // 自定义参数服务器
    "passon" : 0, //透传标志位
    "plate" : 0, //识别码标志位
    "convert" : 0, //hex转换标志位
    "reg" : 0, // 登陆注册包
    "param_ver" : 0, // 参数版本
    "flow" : 0, // 流量监控
    "fota" : 0, // 远程升级
    "uartReadTime" : 50, // 串口读超时
    "netReadTime" : 50, // 网络读超时
    "pwrmod" : "normal",
    "password" : "",
    "upprot" : [], // 上行自定义协议
    "dwprot" : [], // 下行自定义协议
    "apn" : ["", "", ""], // 用户自定义APN
    "cmds" : [], // 自动采集任务参数
    "pins" : ["", "", ""], // 用户自定义IO: netled,netready,rstcnf,
    "conf" : [[], [], [], [], [], [], []], // 用户通道参数
    "preset" : {"number" : "", "delay" : 1, "smsword" : "SMS_UPDATE"}, // 用户预定义的来电电话,延时时间,短信关键字
    "uconf" : [[1, "115200", 8, 2, 0, ""], [2, "115200", 8, 2, 0, ""]], // 串口配置表
    "gps" : {
        "fun" : ["", "115200", "0", "5", "1", "json", "100", ";", "60"], // 用户捆绑GPS的串口,波特率，功耗模式，采集间隔,采集方式支持触发和持续, 报文数据格式支持 json 和 hex，缓冲条数,分隔符,状态报文间隔
        "pio" : ["", "", "", "", "0", "16"] // 配置GPS用到的IO: led脚，vib震动输入脚，ACC输入脚,内置电池充电状态监视脚,adc通道,分压比
    },
    "warn" : {
        "gpio" : [],
        "adc0" : [],
        "adc1" : [],
        "vbatt" : []
    },
    "task" : [] // 用户自定义任务列表
};

// 从设备实际读取的默认值
default_conf = {"flow":0,"passon":0,"warn":{"adc1":{},"adc0":{},"gpio":{},"vbatt":{}},"netReadTime":50,"convert":0,"host":"","upprot":{},"apn":{},"pins":["","",""],"plate":0,"fota":0,"gps":{"fun":["","115200","0","5","1","json","100",";","60"],"pio":["","","","","0","16"]},"param_ver":0,"reg":0,"preset":{"number":"","delay":1,"smsword":"SMS_UPDATE"},"uconf":[[1,115200,8,2,0],[2,115200,8,2,0]],"password":"","task":{},"conf":[[],[],[],[],[],[],[]],"cmds":[{},{}],"pwrmod":"normal","dwprot":{},"uartReadTime":50};
default_conf["nolog"] = "0"
