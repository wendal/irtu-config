# irtu-config

#### 介绍
iRTU的配置工具

#### 软件架构

nwjs + vue + element-ui

可本地运行, 也可以当普通网页访问(无串口读写功能)

## 网页运行

1. 使用nginx
2. 使用 `python2 -m SimpleHTTPServer 8000`
3. 使用 `python -m http.server 80`

## nwjs运行

首先, 下载 nwjs sdk, https://nwjs.io/

解压后, 把nwjs的全部内容拷贝到本项目

双击 nw.exe, 即可启动

![](run_at_nwjs.jpg)
